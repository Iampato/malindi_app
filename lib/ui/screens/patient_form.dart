import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:malindi_sub_county_app/models/input_fields.dart';
import 'package:malindi_sub_county_app/ui/widgets/custom_shape.dart';
import 'package:malindi_sub_county_app/ui/widgets/customappbar.dart';
import 'package:malindi_sub_county_app/ui/widgets/responsive_ui.dart';
import 'package:malindi_sub_county_app/ui/widgets/textformfield.dart';

class PatientForm extends StatefulWidget {
  final Model patientForm;

  const PatientForm({this.patientForm});

  @override
  _PatientFormState createState() => _PatientFormState();
}

class _PatientFormState extends State<PatientForm> {
  final _formKey = GlobalKey<FormState>();
  AddPatient addPatient = AddPatient();
  Model model = Model();
  double _inputHeight = 50;
  final TextEditingController _textEditingController = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    selectedValue = 0;
    selectedval = 0;
    _textEditingController.addListener(_checkInputHeight);
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  void _checkInputHeight() async {
    int count = _textEditingController.text.split('\n').length;

    if (count == 0 && _inputHeight == 50.0) {
      return;
    }
    if (count <= 5) {
      // use a maximum height of 6 rows
      // height values can be adapted based on the font size
      var newHeight = count == 0 ? 50.0 : 28.0 + (count * 18.0);
      setState(() {
        _inputHeight = newHeight;
      });
    }
  }

  bool checkBoxValue = false;
  double _height;
  double _width;
  double _pixelRatio;
  bool _large;
  bool _medium;
  bool monVal = false;
  bool tuVal = false;
  bool wedVal = false;
  bool thurdVal = false;

  int selectedValue;
  int selectedval;

  setSeletedValue(int val) {
    setState(() {
      selectedValue = val;
    });
  }

  setSelectedVal(int one) {
    setSelectedVal(one);
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    _large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    _medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(0xffebecec),
      body: Container(
        height: _height,
        width: _width,
        margin: EdgeInsets.only(bottom: 5),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Opacity(opacity: 0.88, child: CustomAppBar()),
              clipShape(),
              form(),
              SizedBox(
                height: 10,
              ),
              dynamicCheckBox(),
              SizedBox(
                height: _height / 35,
              ),
              button(),
              // infoTextRow(),
              // socialIconsRow(),
              //signInTextRow(),
            ],
          ),
        ),
      ),
    );
  }

  Widget clipShape() {
    return Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.75,
          child: ClipPath(
            clipper: CustomShapeClipper(),
            child: Container(
              height: _large
                  ? _height / 8
                  : (_medium ? _height / 7 : _height / 6.5),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xFFda8b23),
                    Color(0xFFd0e5f8),
                  ],
                ),
              ),
            ),
          ),
        ),
        Opacity(
          opacity: 0.5,
          child: ClipPath(
            clipper: CustomShapeClipper2(),
            child: Container(
              height: _large
                  ? _height / 12
                  : (_medium ? _height / 11 : _height / 10),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xFFda8b23),
                    Color(0xFFd0e5f8),
                  ],
                ),
              ),
            ),
          ),
        ),
        Container(
          height: _height / 5.5,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  spreadRadius: 0.0,
                  color: Colors.black26,
                  offset: Offset(1.0, 10.0),
                  blurRadius: 20.0),
            ],
            color: Colors.white,
            shape: BoxShape.circle,
          ),
          child: GestureDetector(
              onTap: () {
                print('Adding photo');
              },
              child: Icon(
                Icons.add_a_photo,
                size: _large ? 40 : (_medium ? 33 : 31),
                color: Colors.orange[200],
              )),
        ),
      ],
    );
  }

  Widget form() {
    return Container(
      margin: EdgeInsets.only(
          left: _width / 12.0, right: _width / 12.0, top: _height / 20.0),
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            firstNameTextFormField(),
            SizedBox(height: _height / 60.0),
            lastNameTextFormField(),
            SizedBox(height: _height / 60.0),
            phoneTextFormField(),
            SizedBox(height: _height / 60.0),
            residenceTextFormField(),
            SizedBox(height: _height / 60.0),
            bodytemptextFormField(),
            SizedBox(height: _height / 60.0),
            respiratoryRateTextFormField(),
            SizedBox(height: _height / 60.0),
            commentBoxTextField()
          ],
        ),
      ),
    );
  }

  Widget firstNameTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.text,
      icon: Icons.person,
      hint: "First Name",
      validator: (String value) {
        if (value.isEmpty) {
          return "Field Required";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        addPatient.firstName = value;
      },
    );
  }

  Widget lastNameTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.text,
      icon: Icons.person,
      hint: "Last Name",
      validator: (String value) {
        if (value.isEmpty) {
          return "Field Required";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        addPatient.lastName = value;
      },
    );
  }

  Widget ageTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.numberWithOptions(),
      icon: Icons.person,
      hint: "Age",
      validator: (String value) {
        if (value.isEmpty) {
          return "Field Required";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        addPatient.age = value;
      },
    );
  }

  Widget phoneTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.number,
      icon: Icons.phone,
      hint: "Mobile Number",
      validator: (String value) {
        if (value.isEmpty) {
          return "Field Required";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        addPatient.mobileNo = value;
      },
    );
  }

  Widget residenceTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.text,
      obscureText: true,
      icon: Icons.home,
      hint: "Residence",
      validator: (String value) {
        if (value.isEmpty) {
          return "Field Required";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        addPatient.residence = value;
      },
    );
  }

  Widget bodytemptextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.text,
      obscureText: true,
      icon: Icons.theaters,
      hint: "Body Temperature",
      validator: (String value) {
        if (value.isEmpty) {
          return "Field Required";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        addPatient.bodyTemp = value;
      },
    );
  }

  Widget respiratoryRateTextFormField() {
    return CustomTextField(
        keyboardType: TextInputType.text,
        obscureText: true,
        icon: Icons.rate_review,
        hint: "Respiratoty Rate",
        validator: (String value) {
          if (value.isEmpty) {
            return "Field Required";
          }
          _formKey.currentState.save();
          return null;
        },
        onSaved: (String value) {
          addPatient.respireRate = value;
        });
  }

  Widget commentTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.multiline,
      obscureText: true,
      icon: Icons.rate_review,
      hint: "comment",
      validator: (String value) {
        if (value.isEmpty) {
          return "Field Required";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        addPatient.comment = value;
      },
    );
  }

  Widget commentBoxTextField() {
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.black.withOpacity(0.2),
          blurRadius: 10,
        )
      ], borderRadius: BorderRadius.circular(30), color: Colors.white),
      child: TextField(
        decoration: InputDecoration(
          icon: Icon(Icons.comment, size: 20),
          hintText: "Comment",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
            borderSide: BorderSide.none,
          ),
        ),
        controller: _textEditingController,
        textInputAction: TextInputAction.newline,
        keyboardType: TextInputType.multiline,
        maxLines: null,
        onSubmitted: (String value) {
          addPatient.comment = value;
        },
      ),
    );
  }

  Widget buttonInwell() {
    return Container(
        child: InkWell(
            onTap: () {
              setState(() {
                checkBoxValue = !checkBoxValue;
              });
            },
            child: checkBoxValue
                ? Icon(
                    Icons.check,
                    size: 30.0,
                    color: Colors.black,
                  )
                : Icon(
                    Icons.check_box_outline_blank,
                    size: 30.0,
                    color: Colors.blue,
                  )));
  }

  Widget circularCheckBox() {
    return Container(
        decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
        child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: checkBoxValue
                ? Icon(
                    Icons.check,
                    size: 30.0,
                    color: Colors.white,
                  )
                : Icon(
                    Icons.check_box_outline_blank,
                    size: 30.0,
                    color: Colors.blue,
                  )));
  }

  Widget dynamicCheckBox() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Sore Throat"),
            Row(
              children: <Widget>[
                Text("Yes"),
                Radio(
                  value: 1,
                  groupValue: selectedValue,
                  onChanged: (int val) {
                    setState(() {
                      setSeletedValue(val);
                    });
                  },
                ),
                Text("No"),
                Radio(
                  value: 2,
                  groupValue: selectedValue,
                  onChanged: (int val) {
                    setSeletedValue(val);
                  },
                ),
              ],
            ),
          ],
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Cough"),
            Row(
              children: <Widget>[
                Text("Yes"),
                Radio(
                  value: 2,
                  groupValue: selectedval,
                  onChanged: (int one) {
                    setSelectedVal(one);
                  },
                ),
                Text("No"),
                Radio(
                  value: 2,
                  groupValue: selectedval,
                  onChanged: (int one) {
                    setSelectedVal(one);
                  },
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }

  Widget acceptTermsTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 100.0),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topLeft,
            child: Checkbox(
                activeColor: Colors.orange[200],
                value: checkBoxValue,
                onChanged: (bool newValue) {
                  setState(() {
                    checkBoxValue = newValue;
                  });
                }),
          ),
          Align(
            alignment: Alignment.center,
            child: Checkbox(
                activeColor: Colors.orange[200],
                value: checkBoxValue,
                onChanged: (bool newValue) {
                  setState(() {
                    checkBoxValue = newValue;
                  });
                }),
          ),
          Checkbox(
              activeColor: Colors.orange[200],
              value: checkBoxValue,
              onChanged: (bool newValue) {
                setState(() {
                  checkBoxValue = newValue;
                });
              }),
          Text(
            "I accept all terms and conditions",
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: _large ? 12 : (_medium ? 11 : 10)),
          ),
        ],
      ),
    );
  }

  Widget button() {
    return RaisedButton(
      elevation: 0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      onPressed: () {
        _submit();

        // print("$_submit");
        print("object");
        // Navigator.pushNamed(context, ROUND_CHECKBOX);
      },
      textColor: Colors.white,
      padding: EdgeInsets.all(0.0),
      child: Container(
        alignment: Alignment.center,
//        height: _height / 20,
        width: _large ? _width / 4 : (_medium ? _width / 3.75 : _width / 3.5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          gradient: LinearGradient(
            colors: <Color>[
              Color(0xFFda8b23),
              Color(0xFFd0e5f8),
            ],
          ),
        ),
        padding: const EdgeInsets.all(12.0),
        child: Text(
          'SUBMIT',
          style: TextStyle(fontSize: _large ? 14 : (_medium ? 12 : 10)),
        ),
      ),
    );
  }

  Widget infoTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 40.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Or create using social media",
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: _large ? 12 : (_medium ? 11 : 10)),
          ),
        ],
      ),
    );
  }

  Widget socialIconsRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 80.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          CircleAvatar(
            radius: 15,
            backgroundImage: AssetImage("assets/images/googlelogo.png"),
          ),
          SizedBox(
            width: 20,
          ),
          CircleAvatar(
            radius: 15,
            backgroundImage: AssetImage("assets/images/fblogo.jpg"),
          ),
          SizedBox(
            width: 20,
          ),
          CircleAvatar(
            radius: 15,
            backgroundImage: AssetImage("assets/images/twitterlogo.jpg"),
          ),
        ],
      ),
    );
  }

  Widget signInTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Already have an account?",
            style: TextStyle(fontWeight: FontWeight.w400),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: () {
              // Navigator.of(context).pop(SIGN_IN);
              print("Routing to Sign up screen");
            },
            child: Text(
              "Sign in",
              style: TextStyle(
                  fontWeight: FontWeight.w800,
                  color: Colors.orange[200],
                  fontSize: 19),
            ),
          )
        ],
      ),
    );
  }

  void _submit() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      print(addPatient.firstName);
      print(addPatient.lastName);
      print(addPatient.mobileNo);
      print(addPatient.residence);
      print(addPatient.bodyTemp);
      print(addPatient.respireRate);
      print(addPatient.age);
      print(addPatient.comment);

      _addaPatient(
        addPatient.firstName,
        addPatient.lastName,
        addPatient.mobileNo,
        addPatient.residence,
        addPatient.bodyTemp,
        addPatient.respireRate,
        addPatient.age,
        addPatient.comment,
      );

      // If the form is valid, display a Snackbar.
      final snackBar = SnackBar(content: Text('Your Data has been Captured .'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void _addaPatient(
    String firstName,
    String lastName,
    String mobileNo,
    String residence,
    String bodyTemp,
    String respireRate,
    String age,
    String comment,
  ) async {
    String url = "http://165.22.117.80/malindi_client/patient/create";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "X-AUTHENTICATION": ""
    };
    // String payload = '{"firstName":"' +
    //     firstName +
    //     '","lastname":"' +
    //     lastName +
    //     '", "mobileNo":"' +
    //     mobileNo +
    //     '","residence":"' +
    //     residence +
    //     '", "respireRate":"' +
    //     respireRate +
    //     '","age":"' +
    //     age +
    //     '","comment":"' +
    //     comment +
    //     '",}';
  
    String payload = '{"firstName": "$firstName","lastName": "$lastName","mobileNo": "$mobileNo","residence": "$residence","bodyTemp" : "$bodyTemp","respireRate" : "$respireRate","age": "$age",	    "comment": "$comment"}';
    print(payload);

    Response response = await post(url, headers: headers, body: payload);
    //checking the status code for the results
    var decodedResponseBody = json.decode(response.body);
    print(response.body);
    print(response.statusCode);
    if (decodedResponseBody["status"] == 201) {
      Model myModel = Model.fromJson(decodedResponseBody["message"]);
      // Patient added Succefull
      Navigator.pop(context);

      final snackBar =
          SnackBar(content: Text('$myModel created successfully.'));

      _scaffoldKey.currentState.showSnackBar(snackBar);
      print('$myModel');
    } else {
      final snackBar = SnackBar(content: Text('error in creating'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      // error occured
      print(response.body);
    }
  }
}
