import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:malindi_sub_county_app/constants/constants.dart';
import 'package:malindi_sub_county_app/models/signup_model.dart';
import 'package:malindi_sub_county_app/ui/widgets/custom_shape.dart';
import 'package:malindi_sub_county_app/ui/widgets/customappbar.dart';
import 'package:malindi_sub_county_app/ui/widgets/responsive_ui.dart';
import 'package:malindi_sub_county_app/ui/widgets/textformfield.dart';
import 'package:validators/validators.dart' as validator;

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

Future<UserModel> creatUser(
    String firstName,
    String lastName,
    String jobDescription,
    String facilityName,
    String facilityCode,
    String email,
    String password) async {
  final apiUrl = 'http://165.22.117.80/malindi_client/users/create';
  final response = await http.post(apiUrl, body: {
    "firstName": firstName,
    "lastName": lastName,
    "jobDescription": jobDescription,
    "facilityName": facilityName,
    "facilityCode": facilityCode,
    "email": email,
    "password": password,
  });
  if (response.statusCode == 201) {
    final String responseString = response.body;
    print(response.body);
    return userModelFromJson(responseString);
  }
  return null;
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();
  UserModel _user;

  final TextEditingController fistnameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController jobDescriptionController =
      TextEditingController();
  final TextEditingController facilityNameController = TextEditingController();
  final TextEditingController facilityCodeController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  // CreateUser createUser = CreateUser();
  bool checkBoxValue = false;
  double _height;
  double _width;
  double _pixelRatio;
  bool _large;
  bool _medium;

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    _large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    _medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);

    return Material(
      color: Color(0xffebecec),
      child: Scaffold(
        body: Container(
          height: _height,
          width: _width,
          margin: EdgeInsets.only(bottom: 5),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Opacity(opacity: 0.88, child: CustomAppBar()),
                clipShape(),
                form(),
                acceptTermsTextRow(),
                SizedBox(
                  height: _height / 35,
                ),
                button(),
                infoTextRow(),
                testButton(),
                socialIconsRow(),
                //signInTextRow(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget testButton() {
    return RaisedButton(
      elevation: 0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      onPressed: () {
        Navigator.of(context).pushNamed(PATIENT_FORM);
        print("Routing to your account");
      },
      textColor: Colors.white,
      padding: EdgeInsets.all(0.0),
      child: Container(
        alignment: Alignment.center,
//        height: _height / 20,
        width: _large ? _width / 4 : (_medium ? _width / 3.75 : _width / 3.5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          gradient: LinearGradient(
            colors: <Color>[
              Color(0xFFda8b23),
              Color(0xFFd0e5f8),
            ],
          ),
        ),
        padding: const EdgeInsets.all(12.0),
        child: Text(
          'TEST',
          style: TextStyle(fontSize: _large ? 14 : (_medium ? 12 : 10)),
        ),
      ),
    );
  }

  Widget clipShape() {
    return Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.75,
          child: ClipPath(
            clipper: CustomShapeClipper(),
            child: Container(
              height: _large
                  ? _height / 8
                  : (_medium ? _height / 7 : _height / 6.5),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xFFda8b23),
                    Color(0xFFd0e5f8),
                  ],
                ),
              ),
            ),
          ),
        ),
        Opacity(
          opacity: 0.5,
          child: ClipPath(
            clipper: CustomShapeClipper2(),
            child: Container(
              height: _large
                  ? _height / 12
                  : (_medium ? _height / 11 : _height / 10),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xFFda8b23),
                    Color(0xFFd0e5f8),
                  ],
                ),
              ),
            ),
          ),
        ),
        Container(
          height: _height / 5.5,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  spreadRadius: 0.0,
                  color: Colors.black26,
                  offset: Offset(1.0, 10.0),
                  blurRadius: 20.0),
            ],
            color: Colors.white,
            shape: BoxShape.circle,
          ),
          child: GestureDetector(
              onTap: () {
                print('Adding photo');
              },
              child: Icon(
                Icons.add_a_photo,
                size: _large ? 40 : (_medium ? 33 : 31),
                color: Colors.orange[200],
              )),
        ),
      ],
    );
  }

  Widget form() {
    return Container(
      margin: EdgeInsets.only(
          left: _width / 12.0, right: _width / 12.0, top: _height / 20.0),
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            firstNameTextFormField(),
            SizedBox(height: _height / 60.0),
            lastNameTextFormField(),
            SizedBox(height: _height / 60.0),
            jobDescriptionTextFormField(),
            SizedBox(height: _height / 60.0),
            facilityNameTextFormField(),
            SizedBox(height: _height / 60.0),
            facilityCodeTextFormField(),
            emailTextFormField(),
            SizedBox(height: _height / 60.0),
            passwordTextFormField(),
            SizedBox(
              height: 32,
            ),
            _user == null
                ? Container()
                : Text(
                    "The user ${_user.firstName}, ${_user.lastName},${_user.jobDescription}, ${_user.id} is created successfully at time ${_user.created.toIso8601String()}"),
          ],
        ),
      ),
    );
  }

  Widget firstNameTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.text,
      icon: Icons.person,
      hint: "First Name",
      textEditingController: fistnameController,
      // validator: (input) => input.isEmpty(input) ? 'Enter First Name' : null,
      // onSaved: (input) => _user.firstName = input,
    );
  }

  Widget lastNameTextFormField() {
    return CustomTextField(
        keyboardType: TextInputType.text,
        icon: Icons.person,
        hint: "Last Name",
        textEditingController: lastNameController,
        validator: (String value) {
          if (value.isEmpty) {
            return "Enter last Name";
          }
          _formKey.currentState.save();
          return null;
        },
        onSaved: (String value) {
          _user.lastName = value;
        }
        // validator: (input) => input.isEmpty(input) ? 'Enter Last Name' : null,
        // onSaved: (input) => _user.lastName = input,
        );
  }

  Widget jobDescriptionTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.text,
      icon: Icons.work,
      hint: "Job Designation",
      textEditingController: jobDescriptionController,
      validator: (String value) {
        if (value.isEmpty) {
          return "Enter Job Description";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        _user.jobDescription = value;
      },
    );
  }

  Widget facilityNameTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.number,
      icon: Icons.home,
      hint: "Facility Name",
      textEditingController: facilityNameController,
      validator: (String value) {
        if (value.isEmpty) {
          return "Enter Facility Name";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        _user.facilityName = value;
      },
    );
  }

  Widget facilityCodeTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.text,
      obscureText: true,
      icon: Icons.lock,
      hint: "Facility Code",
      textEditingController: facilityCodeController,
      validator: (String value) {
        if (value.isEmpty) {
          return "Enter Facility Code";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        _user.facilityCode = value;
      },
    );
  }

  Widget emailTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.emailAddress,
      obscureText: true,
      icon: Icons.email,
      hint: "Email",
      textEditingController: emailController,
      isEmail: true,
      validator: (String value) {
        if (!validator.isEmail(value)) {
          return "Enter Your Email";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        _user.email = value;
      },
    );
  }

  Widget passwordTextFormField() {
    return CustomTextField(
      keyboardType: TextInputType.emailAddress,
      obscureText: true,
      icon: Icons.lock,
      hint: "Password",
      textEditingController: passwordController,
      validator: (String value) {
        if (value.isEmpty) {
          return "Enter Your Password";
        }
        _formKey.currentState.save();
        return null;
      },
      onSaved: (String value) {
        _user.password = value;
      },
    );
  }

  Widget acceptTermsTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 100.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Checkbox(
              activeColor: Colors.orange[200],
              value: checkBoxValue,
              onChanged: (bool newValue) {
                setState(() {
                  checkBoxValue = newValue;
                });
              }),
          Text(
            "I accept all terms and conditions",
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: _large ? 12 : (_medium ? 11 : 10)),
          ),
        ],
      ),
    );
  }

  Widget button() {
    return RaisedButton(
      elevation: 0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      onPressed: () async {
        final String firstName = fistnameController.text;
        final String lastName = lastNameController.text;
        final String jobDescription = jobDescriptionController.text;
        final String facilityName = facilityNameController.text;
        final String facilityCode = facilityCodeController.text;
        final String email = emailController.text;
        final String password = passwordController.text;

        final UserModel user = await creatUser(firstName, lastName,
            jobDescription, facilityName, facilityCode, email, password);

        setState(() {
          _user = user;
        });
      },
      textColor: Colors.white,
      padding: EdgeInsets.all(0.0),
      child: Container(
        alignment: Alignment.center,
//        height: _height / 20,
        width: _large ? _width / 4 : (_medium ? _width / 3.75 : _width / 3.5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          gradient: LinearGradient(
            colors: <Color>[
              Color(0xFFda8b23),
              Color(0xFFd0e5f8),
            ],
          ),
        ),
        padding: const EdgeInsets.all(12.0),
        child: Text(
          'SIGN UP',
          style: TextStyle(fontSize: _large ? 14 : (_medium ? 12 : 10)),
        ),
      ),
    );
  }

  Widget infoTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 40.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Or create using social media",
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: _large ? 12 : (_medium ? 11 : 10)),
          ),
        ],
      ),
    );
  }

  Widget socialIconsRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 80.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          CircleAvatar(
            radius: 15,
            backgroundImage: AssetImage("assets/images/googlelogo.png"),
          ),
          SizedBox(
            width: 20,
          ),
          CircleAvatar(
            radius: 15,
            backgroundImage: AssetImage("assets/images/fblogo.jpg"),
          ),
          SizedBox(
            width: 20,
          ),
          CircleAvatar(
            radius: 15,
            backgroundImage: AssetImage("assets/images/twitterlogo.jpg"),
          ),
        ],
      ),
    );
  }

  Widget signInTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Already have an account?",
            style: TextStyle(fontWeight: FontWeight.w400),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).pop(SIGN_IN);
              print("Routing to Sign up screen");
            },
            child: Text(
              "Sign in",
              style: TextStyle(
                  fontWeight: FontWeight.w800,
                  color: Colors.orange[200],
                  fontSize: 19),
            ),
          )
        ],
      ),
    );
  }

  void _submit() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      print(_user.firstName);
      print(_user.lastName);
      print(_user.jobDescription);
      print(_user.facilityName);
      print(_user.facilityCode);
      print(_user.email);
      print(_user.password);
      // String username = emailController.text;
      // String password = '123£';
      // String basicAuth =
      //     'Basic ' + base64Encode(utf8.encode('$username:$password'));
      // print(basicAuth);

      // Response r = await post('https://api.somewhere.io',
      //     headers: <String, String>{'authorization': basicAuth});
      // print(r.statusCode);
      // print(r.body);

      // If the form is valid, display a Snackbar.
      Scaffold.of(context).showSnackBar(
          SnackBar(content: Text('Your Data has been Captured .')));
    }
    final String firstName = fistnameController.text;
    final String lastName = lastNameController.text;
    final String jobDescription = jobDescriptionController.text;
    final String facilityName = facilityNameController.text;
    final String facilityCode = facilityCodeController.text;
    final String email = emailController.text;
    final String password = passwordController.text;

    final UserModel user = await creatUser(firstName, lastName, jobDescription,
        facilityName, facilityCode, email, password);

    setState(() {
      _user = user;
    });
  }
}
