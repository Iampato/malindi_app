import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:malindi_sub_county_app/constants/constants.dart';
import 'package:malindi_sub_county_app/ui/widgets/animatetd.dart';

//import 'package:flutter/services.dart';
class GetStatedPage extends StatefulWidget {
  @override
  _GetStatedPageState createState() => _GetStatedPageState();
}

class _GetStatedPageState extends State<GetStatedPage>
    with SingleTickerProviderStateMixin {
  final int delayedAmount = 500;
  double _scale;
  AnimationController _controller;
  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 200,
      ),
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final color = Colors.black45;
    _scale = 1 - _controller.value;

    var gestureDetector = GestureDetector(
      onTapDown: _onTapDown,
      onTapUp: _onTapUp,
      child: Transform.scale(
        scale: _scale,
        child: _animatedButtonUI,
      ),
    );
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(0xffebecec),
          // (0xFF8185E2),
          body: SingleChildScrollView(
            child: Center(
              child: Column(
                children: <Widget>[
                  AvatarGlow(
                    endRadius: 90,
                    duration: Duration(seconds: 2),
                    glowColor: Colors.white24,
                    repeat: true,
                    repeatPauseDuration: Duration(seconds: 2),
                    startDelay: Duration(seconds: 1),
                    child: Material(
                        elevation: 8.0,
                        shape: CircleBorder(),
                        child: CircleAvatar(
                          backgroundColor: Colors.grey[100],
                          child: Image.asset(
                            'assets/images/kilifi-logo.jpg',
                          ),
                          radius: 50.0,
                        )),
                  ),
                  DelayedAnimation(
                    child: Text(
                      "Department of Health",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                          color: color),
                    ),
                    delay: delayedAmount + 1000,
                  ),
                  DelayedAnimation(
                    child: Text(
                      "County Government of Kilifi",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                          color: color),
                    ),
                    delay: delayedAmount + 2000,
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  DelayedAnimation(
                    child: Text(
                      "Facility COVID-19 TRIAGEDATA",
                      style: TextStyle(fontSize: 15.0, color: color),
                    ),
                    delay: delayedAmount + 3000,
                  ),
                  DelayedAnimation(
                    child: Text(
                      "COLLECTION & REPORTINGTOOL",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 15.0, color: color),
                    ),
                    delay: delayedAmount + 3000,
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                  DelayedAnimation(
                    child: gestureDetector,
                    delay: delayedAmount + 4000,
                  ),
                  SizedBox(
                    height: 50.0,
                  ),
                  DelayedAnimation(
                    child: Text(
                      "God Above All".toUpperCase(),
                      style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          color: color),
                    ),
                    delay: delayedAmount + 5000,
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  Widget get _animatedButtonUI => Container(
        height: 60,
        width: 270,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          gradient: LinearGradient(
            colors: <Color>[Color(0xFFda8b23), Color(0xFFd0e5f8)],
          ),
        ),
        child: Center(
          child: Text(
            'Get Started',
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Color(0xFFda8b23)),
          ),
        ),
      );

  void _onTapDown(TapDownDetails details) {
    _controller.forward();
    Navigator.of(context).pushNamed(SIGN_IN);
  }

  void _onTapUp(TapUpDetails details) {
    _controller.reverse();
  }
}
