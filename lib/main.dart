import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:malindi_sub_county_app/ui/screens/signin.dart';
import 'package:malindi_sub_county_app/ui/screens/signup.dart';
import 'package:malindi_sub_county_app/ui/screens/splash-screen2.dart';


import 'constants/constants.dart';
import 'ui/screens/patient_form.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Login",
      theme: ThemeData(primaryColor: Colors.orange[200]),
      routes: <String, WidgetBuilder>{
        SPLASH_SCREEN: (BuildContext context) =>  GetStatedPage(),
        SIGN_IN: (BuildContext context) =>  SignInPage(),
        SIGN_UP: (BuildContext context) =>  SignUpScreen(),
        PATIENT_FORM: (BuildContext context) =>  PatientForm(),
        // ROUND_CHECKBOX: (BuildContext context) =>  RoundChekBox(),
      },
      initialRoute: SPLASH_SCREEN,
    );
  }
}



