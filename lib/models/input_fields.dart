import 'dart:convert';
import 'package:http/http.dart' as http;
class Model {
  String id;
  String firstName;
  String lastName;
  String username;
  String password;
  String jobDescription;
  String facilityName;
  String gender;
  String facilityCode;
  String mobileNo;
  String residence;
  String bodyTemp;
  String respirRate;
  String  age;
  String comement;

  Model({
    this.id,
    this.firstName,
    this.lastName,
    this.username,
    this.password,
    this.jobDescription,
    this.facilityName,
    this.gender,
    this.mobileNo,
    this.residence,
    this.bodyTemp,
    this.respirRate,
    this.age,
    this.comement
  });
    factory Model.fromJson(Map<String, dynamic> json) => Model(
        id: json["id"],
        firstName: json["first_name"],
        lastName: json["middle_name"],
        username: json["username"],
        jobDescription: json["jodescription"],
        facilityName: json["facilityName"],
        gender: json["gender"],
        mobileNo: json["gender"],
        residence: json["residence"],
        bodyTemp: json["bodyTemp"],
        respirRate: json["respirRate"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "lastName": lastName,
        "username": username,
        "jobDescription": jobDescription,
        "facilityName": facilityName,
        "gender": gender,
        "mobileNo": mobileNo,
        "residence": residence,
        "bodyTemp": bodyTemp,
        "respirRate": respirRate,
    };
    
}
Future<Model> createModel(String firstName, lastName,mobileNo,residence,bodTemp,respirRate)async{
  final http.Response response = await http.post(
    'https://jsonplaceholder.type.com/patient',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'firstName': firstName,
      'lastName': lastName,
      'mobileNo': mobileNo,
      'residence':residence,
      'bodTemp': bodTemp,
      'respirRate': respirRate,
    }),
  );
  if(response.statusCode ==201){
    return Model.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to load Patient record');
  }
}
class CreateUser {
    String firstName;
    String lastName;
    String jobDescription;
    String facilityName;
    String facilityCode;
    String email;
    String password;

    CreateUser({
        this.firstName,
        this.lastName,
        this.jobDescription,
        this.facilityName,
        this.facilityCode,
        this.email,
        this.password,
    });

    factory CreateUser.fromJson(Map<String, dynamic> json) => CreateUser(
        firstName: json["firstName"],
        lastName: json["lastName"],
        jobDescription: json["jobDescription"],
        facilityName: json["facilityName"],
        facilityCode: json["facilityCode"],
        email: json["email"],
        password: json["password"],
    );

    Map<String, dynamic> toJson() => {
        "firstName": firstName,
        "lastName": lastName,
        "jobDescription": jobDescription,
        "facilityName": facilityName,
        "facilityCode": facilityCode,
        "email": email,
        "password": password,
    };
}
Future<CreateUser> createPost(String url, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;
 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return CreateUser.fromJson(json.decode(response.body));
  });
}

Future<CreateUser> createUser(String firstName, lastName,jobDescription,facilityName,facilityCode,email)async{
  final http.Response response = await http.post(
    'http://localhost/bible-study/users/create',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'firstName': firstName,
      'lastName': lastName,
      'jobDescription': jobDescription,
      'facilityName' : facilityName,
      'facilityCode' : facilityCode,
      'email' : email

    }),
  );
  if(response.statusCode ==201){
    return CreateUser.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to load Patient record');
  }
}

class AddPatient {
  String firstName;
  String lastName;
  String mobileNo;
  String residence;
  String bodyTemp;
  String respireRate;
  String age;
  String comment;

  AddPatient(
      {this.firstName,
      this.lastName,
      this.mobileNo,
      this.residence,
      this.bodyTemp,
      this.respireRate,
      this.age,
      this.comment});

  AddPatient.fromJson(Map<String, dynamic> json) {
    firstName = json['firstName'];
    lastName = json['lastName'];
    mobileNo = json['mobileNo'];
    residence = json['residence'];
    bodyTemp = json['bodyTemp'];
    respireRate = json['respireRate'];
    age = json['age'];
    comment = json['comment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['mobileNo'] = this.mobileNo;
    data['residence'] = this.residence;
    data['bodyTemp'] = this.bodyTemp;
    data['respireRate'] = this.respireRate;
    data['age'] = this.age;
    data['comment'] = this.comment;
    return data;
  }
}