// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
    String id;
    String firstName;
    String lastName;
    String jobDescription;
    String facilityName;
    String facilityCode;
    String email;
    String password;
    DateTime created;
    DateTime updated;

    UserModel({
        this.id,
        this.firstName,
        this.lastName,
        this.jobDescription,
        this.facilityName,
        this.facilityCode,
        this.email,
        this.password,
        this.created,
        this.updated,
    });

    factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        jobDescription: json["jobDescription"],
        facilityName: json["facilityName"],
        facilityCode: json["facilityCode"],
        email: json["email"],
        password: json["password"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "lastName": lastName,
        "jobDescription": jobDescription,
        "facilityName": facilityName,
        "facilityCode": facilityCode,
        "email": email,
        "password": password,
        "created": created.toIso8601String(),
        "updated": updated.toIso8601String(),
    };
}
