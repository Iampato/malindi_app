import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThemeColor {
  static const Color background = Color(0XFFFFFFFF);
  static const Color titleTextColor = const Color(0xff5a5d85);
  static const Color subTitleTextColor = const Color(0xff797878);

  static const Color bottonTitleTextColor = const Color(0xffd4d4ea);
}
